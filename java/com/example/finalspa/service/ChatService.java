package com.example.finalspa.service;

import com.example.finalspa.model.Chat;
import com.example.finalspa.repository.ChatRepository;
import com.example.finalspa.service.interfaces.IChatService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService implements IChatService {
    private final ChatRepository chatRepository;

    @Override
    public void add(Chat o) {
        chatRepository.save(o);

    }

    @Override
    public List<Chat> getAll() {
        return chatRepository.findAll();
    }

    @Override
    public Chat update(Chat chat) {
        return chatRepository.save(chat);
    }

    @Override
    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }
}
