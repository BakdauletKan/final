package com.example.finalspa.service;

import com.example.finalspa.model.Auth;
import com.example.finalspa.model.User;
import com.example.finalspa.repository.AuthRepository;
import com.example.finalspa.repository.UserRepository;
import com.example.finalspa.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AuthService implements IAuthService {

    @Autowired
    private AuthRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public User login(String l, String p) {
        Long userId = repository.findAuthByToken(findToken(l, p)).getUserId();
        return userRepository.getById(userId);
    }

    @Override
    public boolean isTokenExist(String t) {
        if(repository.existsByToken(t))
        {
            return true;
        }
        return false;
    }


    @Override
    public void add(Auth o) {
        repository.save(o);
    }

    @Override
    public List<Auth> getAll() {
        return repository.findAll();
    }

    @Override
    public Auth update(Auth auth) {
        repository.deleteById(auth.getId());
        return repository.save(auth);
    }

    @Override
    public void delete(Auth auth) {
        repository.delete(auth);
    }

    private String findToken(String login, String password){
        return repository.findAuthByLoginAndPassword(login, password).getToken();
    }
}