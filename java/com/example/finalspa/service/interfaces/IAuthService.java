package com.example.finalspa.service.interfaces;

import com.example.finalspa.model.Auth;
import com.example.finalspa.model.User;

public interface IAuthService extends IService<Auth> {
    public User login(String l, String p);

    boolean isTokenExist(String t);


}
