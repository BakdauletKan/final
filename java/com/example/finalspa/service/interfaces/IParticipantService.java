package com.example.finalspa.service.interfaces;

import com.example.finalspa.model.Chat;
import com.example.finalspa.model.User;

import java.util.List;

public interface IParticipantService extends IService<Participant> {
    List<Chat> getChatsByUserId(Long userId);
    List<User> getUsersByChatId(Long chatId);

    boolean existsByUserIdAndChatId(Long userid, Long chatid);

}
