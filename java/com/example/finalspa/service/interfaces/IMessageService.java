package com.example.finalspa.service.interfaces;

import com.example.finalspa.model.Message;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IMessageService extends IService<Message> {
    List<Message> findMessagesByChatId(Long chatId);
    //
    List<Message> findAllByChatId(Long id, Integer size);

    List<Message> findAllByUserId(Long requserid, Long id, Integer size);

    Message changeisread(Long userId, Long messageId, boolean status);

    List<Message> getnotdelivered(Long id);
}