package com.example.finalspa.service.interfaces;

import com.example.finalspa.model.User;

public interface IUserService extends IService<User> {

}
