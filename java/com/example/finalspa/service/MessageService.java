package com.example.finalspa.service;

import com.example.finalspa.model.Message;
import com.example.finalspa.repository.MessageRepository;
import com.example.finalspa.service.interfaces.IMessageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService implements IMessageService {

    private final MessageRepository messageRepository;

    @Override
    public void add(Message o) {
        messageRepository.save(o);
    }

    @Override
    public List<Message> getAll() {

        return messageRepository.findAll();
    }

    @Override
    public Message update(Message message) {

        Optional<Message> messageDbOptional = messageRepository.findById(message.getId());
        if(messageDbOptional.isPresent()) {
            Message messageDb = messageDbOptional.get();
            messageDb.setText(message.getText());

            return messageRepository.save(messageDb);
        } else {
            return null;
        }
    }

    @Override
    public void delete(Message message) {
        messageRepository.delete(message);
    }

    @Override
    public List<Message> findMessagesByChatId(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    @Override
    public List<Message> findAllByChatId(Long id, Integer size) {

        return messageRepository.findAllByChatId(id, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    @Override
    public List<Message> findAllByUserId(Long requserid, Long id, Integer size) {
        return messageRepository.findAllByUserId(id, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    @Override
    public Message changeisread(Long userId, Long messageId, boolean status) {
        Message m = messageRepository.findMessageByUserIdAndId(userId, messageId);
        return m;
    }

    @Override
    public List<Message> getnotdelivered(Long id) {

        return messageRepository.findByChatIdAndDeliveredFalse(id);
    }


}
