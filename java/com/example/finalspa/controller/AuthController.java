package com.example.finalspa.controller;

import com.example.finalspa.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private IAuthService authService;

    @RequestMapping("/login/{pass}/{login}")
    public ResponseEntity<?> login(@PathVariable(value = "login") String login, @PathVariable(value = "pass") String pass){
        return authService.login(login,pass) == null ? ResponseEntity.ok(login) : ResponseEntity.ok("No account exist");
    }
}
