package com.example.finalspa.controller;

import com.example.finalspa.model.Chat;
import com.example.finalspa.service.ChatService;
import com.example.finalspa.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/chat")
public class ChatController {
    private final ChatService chatService;

    private final IAuthService authService;

    @GetMapping("/getall")
    public ResponseEntity<?> getAll(@RequestHeader String t) {
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(chatService.getAll());
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Chat chat, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            chatService.add(chat);
            return ResponseEntity.ok("Chat successfully added");
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @PutMapping("/edit")
    public ResponseEntity<?> edit(@RequestBody Chat chat, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            try {
                chatService.update(chat);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
            return ResponseEntity.ok(chat);
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody Chat chat, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            try {
                chatService.delete(chat);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
            return ResponseEntity.ok("Chat successfully deleted");
        }

        return ResponseEntity.ok("Token is not found!!!");
    }
}
