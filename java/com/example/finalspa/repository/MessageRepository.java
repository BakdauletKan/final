package com.example.finalspa.repository;

import com.example.finalspa.model.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findMessagesByChatId(Long chatId);

    List<Message> findAllByChatId(Long id, Pageable pageable);

    List<Message> findAllByUserId(Long id, Pageable pageable);

    Message findMessageByUserIdAndId(Long userId, Long messageId);

    List<Message> findByChatIdAndDeliveredFalse(Long chatid);

}
