package com.example.finalspa.repository;

import com.example.finalspa.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {

    Auth findAuthByLoginAndPassword(String login, String password);

    boolean existsByToken(String token);

    Auth findAuthByToken(String token);

}
