package com.example.finalspa.repository;

import com.example.finalspa.model.Chat;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {

}

